from django.contrib import admin
from django.urls import path, include
from cargurublog.views import list_posts, show_post,show_about,show_contact

urlpatterns = [
    path("all/", list_posts, name="main_page" ), #new
    path("about/",  show_about, name='about'), #new
    path("contact/",  show_contact, name='contact'), #new
    path("post/<slug:slug>/", show_post, name="show_post"),
  ]
