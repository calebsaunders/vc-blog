document.addEventListener("DOMContentLoaded", function () {
  const logoSvg = `
      <svg width="120" height="50" xmlns="http://www.w3.org/2000/svg">
        <defs>
          <linearGradient id="grad1" x1="0%" y1="0%" x2="100%" y2="0%">
            <stop offset="0%" style="stop-color:#3498db;stop-opacity:1" />
            <stop offset="100%" style="stop-color:#2ecc71;stop-opacity:1" />
          </linearGradient>
        </defs>
        <rect width="120" height="50" rx="5" ry="5" fill="url(#grad1)" />
        <text x="50%" y="50%" font-family="Arial, sans-serif" font-size="20" fill="white" text-anchor="middle" alignment-baseline="middle" font-weight="bold">
          VC BLOG
        </text>
      </svg>
    `;

  const nav = document.querySelector(".navigation ul");
  if (nav) {
    const logoLi = document.createElement("li");
    logoLi.innerHTML = `<a href="#" class="logo">${logoSvg}</a>`;
    nav.insertBefore(logoLi, nav.firstChild);
  }
});
