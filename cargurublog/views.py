from django.shortcuts import render , get_object_or_404
from cargurublog.models import Post, Category
from django.db.models import Prefetch

# Create your views here.


def show_about(request):
    return render(request, "cargurublog/about.html")

def show_contact(request):
    return render(request, "cargurublog/contact.html")

def list_posts(request):
    posts = Post.objects.prefetch_related(
        Prefetch('categories', queryset=Category.objects.all())
    ).all()
    context = {
        "post_list": posts
    }

    return render(request, "cargurublog/list_posts.html", context)



def show_post(request, slug):
    post = get_object_or_404(Post, slug=slug)
    categories = post.categories.all()
    context = {
        "post_object": post,
        "categories": categories
    }
    return render(request, "cargurublog/post_detail.html", context=context)
